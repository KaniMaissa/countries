import React from "react";
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";

const About = () => {

    return (
      <div>
        <Navigation />
        <Logo />
        <h1>A Propos</h1>
        <br/>
        <p>Lorem epsum Lorem epsum  Lorem epsum Lorem epsum 
            Lorem epsum Lorem epsum Lorem epsum Lorem epsum 
            Lorem epsum Lorem epsum vLorem epsum Lorem epsum
             Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem 
            epsum Lorem epsum Lorem epsum Lorem epsum Lorem 
            epsum Lorem epsum Lorem epsum 
        </p>
        <br/>
        <p>Lorem epsum Lorem epsum  Lorem epsum Lorem epsum 
            Lorem epsum Lorem epsum Lorem epsum Lorem epsum 
            Lorem epsum Lorem epsum vLorem epsum Lorem epsum
             Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem 
            epsum Lorem epsum Lorem epsum Lorem epsum Lorem 
            epsum Lorem epsum Lorem epsum 
        </p>
        <br/>
      </div>
    );
  
};

export default About;
