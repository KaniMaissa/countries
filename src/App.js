import React,{Component} from "react";
import {BrowserRouter , Routes , Route} from "react-router-dom";
import Home from "./pages/Home";
import About from "./pages/About"

class App extends Component  {
  render(){
  return (
    <div>
   <BrowserRouter>
    <Routes> 
      <Route  path="/" exact  element={<Home/>}  />
      <Route  path="about"  element={<About/>}  />
    </Routes>
   </BrowserRouter>
   </div>
  );
}
}

export default App;
